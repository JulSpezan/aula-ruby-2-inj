=begin
CABEÇALHO DA TAREFA DE RUBY - AULA 2
                
============Implementação do diagrama de classe============

- São 5 classes: Aluno, Usuário, Turma, Professor, Matéria.
- A relação entre classes será feito atráves de arrays.

Metódos:
Usuários
    -idade;
        retorna a idade levando em conta o nascimento
    -logar(senha)
        modificada a variável logado para true, caso esteja certa
    - desloga
        modifica a variável logado para false

Aluno
    - inscrever(nome_turma)
        adiciona a turma do aluno no array turmas

Turma
    - abrir_inscrição
        modifica a variável inscrição_aberta para true
    -fechar_inscrição
        modifica a variável inscrição_aberta para false
    - adicionar_aluno(nome_aluno)
        adiciona um aluno no array de alunos

Professor
    - adiciona_materia(nome_materia)
        adiciona nome_matéria ao array de matérias

Matéria

    - adicionar_professores(nome_professor)
            adiciona professor ao array de professores

============================================================
=end
