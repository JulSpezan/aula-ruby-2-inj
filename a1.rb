# Classe Usuário: inicialização e implementação das funções/metódos 

class Usuario
    senha_correta = "12345"
    ano_agora = 2020

    attr_accessor :email, :senha, :nome, :nascimento 
    def initialize(email, senha, nome, nascimento)
        @email = email
        @senha = senha
        @nome = nome
        @nascimento = nascimento
        @logado = false
    end

    #Função que retorna a idade do usuário
    def retorna_idade
        puts (ano_agora - @nascimento)
        
    end

    #Função que loga no sistema
    def logar
        if (@senha == senha_correta)
            @logado = true
            puts "Logado no sistema"
        else
            @logado = false
            puts "Senha Incorreta"
        end
    end

    #Função que desloga do sistema
    def deslogar
        @logado = false
        puts "Deslogado do sistema"
    end         
end


# Classe Aluno: inicialização e implementação das funções/metódos 

class Aluno < Usuario
    
    attr_accessor :matricula, :periodo, :curso, :turmas
    def initialize(matricula,periodo, curso, turmas)
        @matricula = matricula
        @periodo = periodo
        @curso = curso
        @turmas = []
    end

    #Função que inscreve a turma do aluno no array de turmas
    def inscrever(nome_turma)
        if (inscricao_aberta == true)
            @turmas.push(nome_turma)
        end
    end

end


# Classe Professor: inicialização e implementação das funções/metódos 

class Professor < Usuario
    attr_accessor :matricula, :salario, :materias
    def initialize(matricula, salario, materias)
        @matricula = matricula
        @salario = salario
        @materias = []
    end

    #Função que adiciona nome_matéria ao array de matérias
    def adiciona_materia(nome_materia)
        @materias.push(nome_turma)
    end

end


# Classe Turma: inicialização e implementação das funções/metódos 

class Turma
    attr_accessor :nome, :horario, :dias_da_semana, :inscritos, :inscrição_aberta
    def initialize(nome, horario, dias_da_semana, inscritos, inscricao_aberta)
        @nome = nome
        @horario = horario
        @dias_da_semana = dias_da_semana
        @inscritos = []
        @inscricao_aberta =  inscricao_aberta
    end
    
    #Função que modifica a variável inscricao_aberta para true
    def abrir_inscrição(inscricao_aberta)
        @inscrição_aberta = true
    end

    #Função que modifica a variável inscricao_fechada para false
    def fechar_inscrição(inscricao_aberta)
        @inscrição_aberta = false
    end
    
    #Função que adiciona alunos no array de inscritos
    def adicionar_aluno(nome_aluno)
        @inscritos.push(nome_aluno)
    end
end


# Classe Matéria: inicialização e implementação das funções/metódos 

class Materia
    attr_accessor :ementa, :nome, :professor
    def initialize(ementa, nome, professor)
        @ementa = ementa
        @nome = nome
        @professores = []
    end
    
    #Função que adiciona professor no array de professores
    def adicionar_professores(nome_professor)
        @professores.push(nome_professor)
    end
end


#===============================Entradas===============================

#Usuário

usuario = Usuario.new()
logar
retorna_idade
deslogar

#Aluno

aluno = Aluno.new()
inscrever()
puts @turmas

#Professor
professor = Professor.new()
adiciona_materia()
puts @materias

#Turmas
abrir_inscrição
fechar_inscrição
adicionar_aluno()

#Materias
adicionar_professores
puts @professores
